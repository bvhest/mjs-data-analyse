## Buurten Tilburg

 * Beschrijving: De buurt is een uitsplitsing van een wijk. Deze wordt vooral gebruikt voor de presentatie van statistische gegevens.
 * Bron: gemeente Tilburg
 * Doel registratie: Het geven van (statistische) informatie over de gemeente Tilburg op gebiedsniveau en het indelen van de gemeente voor het gebiedsmanagement
 * Beperkingen: Deze dataset is niet geschikt voor juridische of landmeetkundige doeleinden
 * Mogelijkheden: Deze dataset is geschikt voor het inzichtelijk maken van de locatie op de kaart
 * Coördinatenstelsel: **WGS1984** (EPSG:4326)

## bron 

 * https://ckan.dataplatform.nl/organization/gemeente-tilburg
 * https://ckan.dataplatform.nl/dataset/stadsdelen-tilburg/resource/9e30f5cc-8988-4d9f-974f-a01860d63a72
 * https://ckan.dataplatform.nl/dataset/buurten-tilburg

